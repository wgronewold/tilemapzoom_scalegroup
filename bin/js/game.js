///<reference path = "../tsDefinitions/phaser.d.ts" />
///<reference path = "../tsDefinitions/phaser-tiled.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var codeTemplate;
(function (codeTemplate) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.init = function () {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            if (this.game.device.desktop) {
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.scale.setMinMax(480, 260, 1024, 768);
                this.scale.pageAlignHorizontally = true;
                this.scale.pageAlignVertically = true;
            }
            else {
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.scale.setMinMax(480, 260, 1024, 768);
                this.scale.pageAlignHorizontally = true;
                this.scale.pageAlignVertically = true;
                this.scale.forceOrientation(true, false);
                this.scale.setResizeCallback(this.gameResized, this);
                this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
                this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
            }
            this.game.plugins.add(new Phaser.Plugin.Tiled(this.game));
            // Found this way digging around englercj's Link to the Past phaser port
            //this.add.plugin(new Phaser.Plugin.Tiled(this.game, this.stage));
        };
        Boot.prototype.preload = function () {
        };
        Boot.prototype.create = function () {
            this.game.state.start('Preloader');
        };
        Boot.prototype.gameResized = function () {
        };
        Boot.prototype.enterIncorrectOrientation = function () {
            codeTemplate.orientated = false;
            document.getElementById('orientation').style.display = 'block';
        };
        Boot.prototype.leaveIncorrectOrientation = function () {
            codeTemplate.orientated = true;
            document.getElementById('orientation').style.display = 'none';
        };
        return Boot;
    })(Phaser.State);
    codeTemplate.Boot = Boot;
})(codeTemplate || (codeTemplate = {}));
///<reference path = "../tsDefinitions/phaser.d.ts" />
///<reference path = "../tsDefinitions/phaser-tiled.d.ts" />
var codeTemplate;
(function (codeTemplate) {
    codeTemplate.music = null;
    codeTemplate.orientated = false;
    codeTemplate.buttonPressed = false;
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 1024, 768, Phaser.AUTO, 'content', null);
            this.state.add('Boot', codeTemplate.Boot, false);
            this.state.add('Preloader', codeTemplate.Preloader, false);
            this.state.add('Sim', codeTemplate.Sim, false);
            this.state.start('Boot');
        }
        return Game;
    })(Phaser.Game);
    codeTemplate.Game = Game;
})(codeTemplate || (codeTemplate = {}));
///<reference path = "../tsDefinitions/phaser.d.ts" />
///<reference path = "../tsDefinitions/phaser-tiled.d.ts" />
var codeTemplate;
(function (codeTemplate) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            this.game.time.advancedTiming = true;
            // Phaser Way
            this.load.image('16x16', 'assets/16x16.png');
            this.load.tilemap('map', 'assets/map.json', null, Phaser.Tilemap.TILED_JSON);
            var cacheKey = Phaser.Plugin.Tiled.utils.cacheKey;
            //this.game.load.tiledmap();
            this.load.tiledmap(cacheKey('map', 'tiledmap'), 'assets/map.json', null, Phaser.Tilemap.TILED_JSON);
            this.load.image(cacheKey('map', 'tileset', '16x16'), 'assets/16x16.png');
        };
        Preloader.prototype.create = function () {
        };
        Preloader.prototype.update = function () {
            this.game.state.start('Sim');
        };
        return Preloader;
    })(Phaser.State);
    codeTemplate.Preloader = Preloader;
})(codeTemplate || (codeTemplate = {}));
///<reference path = "../tsDefinitions/phaser.d.ts" />
var codeTemplate;
(function (codeTemplate) {
    var Sim = (function (_super) {
        __extends(Sim, _super);
        function Sim() {
            _super.apply(this, arguments);
        }
        Sim.prototype.create = function () {
            this.stage.backgroundColor = "#4DBD33";
<<<<<<< HEAD
            this.scaling = false;
            // Create the tilemap and the tilelayer
            this.map = this.add.tilemap('map', 16, 16, 96, 72);
            this.map.addTilesetImage('16x16', '16x16');
            //this.map.addTilesetImage('tiles');
            //this.map.setTileSize(32, 32);
            //this.layer = this.map.create('layer', 96, 72, 32, 32);
            //this.layer = this.map.createBlankLayer('2', 96, 72, 32, 32);
            //this.layerPaint = this.map.create('layer', 96, 72, 32, 32 );
            this.layer = this.map.createLayer('layer', 1536, 1152);
            this.layer.resizeWorld();
            // This holds a Tile from the tilemap's tileset
            // Will be used to "paint" the tilemap
            this.currentTile = new Phaser.Tile(this.layer, 4, 0, 0, 16, 16);
=======
            this.map = this.add.tiledmap('map');
            this.layer = this.map.layers[0];
            this.layer.resizeWorld();
            console.log(this.map.widthInPixels);
            console.log(this.map.heightInPixels);
            console.log(this.layer.widthInPixels);
            console.log(this.layer.heightInPixels);
            this.tileSet = this.map.tilesets[0];
            this.currentTile = new Phaser.Plugin.Tiled.Tile(this.game, 0, 0, 4, this.tileSet, this.layer);
            this.currentTileSprite = this.map.tilesets[0].getTileTexture(4);
            //console.log(this.map.tilesets[0].getTileTexture());
            var sprite = this.map.tilesets[0].getTileTexture(4);
            console.log(this.currentTile.tileset.name);
            console.log(this.currentTile.layer.name);
>>>>>>> 4b6fce23a823f9b78a4c8ca2b5126c5e5bc5f416
            this.marker = this.add.graphics(0, 0);
            this.marker.lineStyle(2, 0x000000, 1);
            this.marker.drawRect(0, 0, 16, 16);
            this.marker.height = 16;
            this.marker.width = 16;
            this.input.addMoveCallback(this.updateMarker, this);
            this.cursors = this.input.keyboard.createCursorKeys();
<<<<<<< HEAD
            this.layerScaleGroup = this.add.group();
            this.layerScaleGroup.add(this.layer);
            this.layerScaleGroup.add(this.marker);
            this.layerScale = .66;
            this.inputScale = 2.33;
=======
            this.groupScale = 1;
            this.inputScale = 1;
            this.tileMapGroup = this.game.add.group();
            //this.tileMapGroup.add(this.map);
            this.tileMapGroup.add(this.layer);
            this.tileMapGroup.add(this.marker);
>>>>>>> 4b6fce23a823f9b78a4c8ca2b5126c5e5bc5f416
        };
        Sim.prototype.updateMarker = function () {
            // Draw a Tile Marker and update its position
            // this.marker.x = this.layer.getTileX(this.input.activePointer.x) * 16;
            // this.marker.y = this.layer.getTileY(this.input.activePointer.y) * 16;
            var tile = this.layer.getTiles(this.input.activePointer.worldX, this.input.activePointer.worldY, 16, 16);
            this.marker.x = tile[0].x;
            this.marker.y = tile[0].y;
            if (this.input.mousePointer.isDown) {
                tile[0].setTexture(this.currentTileSprite);
            }
        };
        Sim.prototype.update = function () {
            if (this.input.keyboard.isDown(Phaser.Keyboard.Q)) {
                this.groupScale -= .05;
                this.inputScale += .05;
                this.groupScaled = true;
            }
            if (this.input.keyboard.isDown(Phaser.Keyboard.A)) {
                this.groupScale += .05;
                this.inputScale -= .05;
                this.groupScaled = true;
            }
<<<<<<< HEAD
            if (this.scaling === true) {
                this.layerScale = Phaser.Math.clamp(this.layerScale, .66, 1);
                this.inputScale = Phaser.Math.clamp(this.inputScale, 1, 1.33);
                this.layer.setScale(this.layerScale);
                this.marker.scale.set(this.layerScale, this.layerScale);
                //his.layerScaleGroup.scale.set(this.layerScale);
=======
            if (this.groupScaled === true) {
                this.groupScale = Phaser.Math.clamp(this.groupScale, .67, 1);
                this.inputScale = Phaser.Math.clamp(this.inputScale, 1, 1.5);
                //this.groupScale = Phaser.Math.
                this.tileMapGroup.scale.set(this.groupScale);
                //this.layer.scale.set(this.groupScale);
>>>>>>> 4b6fce23a823f9b78a4c8ca2b5126c5e5bc5f416
                this.input.scale.set(this.inputScale);
                //this.layer.setupRenderArea();
                this.layer.postUpdate();
                this.groupScaled = false;
            }
            if (this.cursors.left.isDown) {
                this.camera.x -= 20;
                console.log('scrolling');
            }
            else if (this.cursors.right.isDown) {
                this.camera.x += 20;
            }
            if (this.cursors.up.isDown) {
                this.camera.y -= 20;
            }
            else if (this.cursors.down.isDown) {
                this.camera.y += 20;
            }
        };
        Sim.prototype.render = function () {
            this.game.debug.cameraInfo(this.camera, 16, 16);
            //this.game.debug.inputInfo( 16, 100);
            //this.game.debug.text("Layer Group: " + this.groupScaleGroup.scale.x, 16, 16);
            var fpsToString = this.game.time.fps;
            this.game.debug.text(fpsToString.toString(), 2, 14, "#00ff00");
            //this.game.debug.cameraInfo(this.game.camera, 32, 32);
        };
        return Sim;
    })(Phaser.State);
    codeTemplate.Sim = Sim;
})(codeTemplate || (codeTemplate = {}));
window.onload = function () {
    var game = new codeTemplate.Game();
};
