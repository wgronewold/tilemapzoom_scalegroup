///<reference path = "../tsDefinitions/phaser.d.ts" />
///<reference path = "../tsDefinitions/phaser-tiled.d.ts" />

module codeTemplate {
	
      
	export class Boot extends Phaser.State {
		
            
            
		init()
                {

                  this.input.maxPointers = 1;
                  this.stage.disableVisibilityChange = true;

                  if (this.game.device.desktop) {
                        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                        this.scale.setMinMax(480, 260, 1024, 768);
                        this.scale.pageAlignHorizontally = true;
                        this.scale.pageAlignVertically = true;
                  }
                  else {
                        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                        this.scale.setMinMax(480, 260, 1024, 768);
                        this.scale.pageAlignHorizontally = true;
                        this.scale.pageAlignVertically = true;
                        this.scale.forceOrientation(true, false);
                        this.scale.setResizeCallback(this.gameResized, this);
                        this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
                        this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
                  }
                  
                  this.game.plugins.add(new Phaser.Plugin.Tiled(this.game));
                  
                  // Found this way digging around englercj's Link to the Past phaser port
                  //this.add.plugin(new Phaser.Plugin.Tiled(this.game, this.stage));
			
			
		}
		
		preload() {
			
			
		}
		
		create() {
			
			this.game.state.start('Preloader');
			
			
		}
            
            gameResized(){
                  
                  
                  
                  
                  
            }
            
            enterIncorrectOrientation() {
                  
                  
                  codeTemplate.orientated = false;
                  document.getElementById('orientation').style.display = 'block';
                        
            }
            
            leaveIncorrectOrientation(){
                  
                  codeTemplate.orientated = true;
                  
                  document.getElementById('orientation').style.display = 'none';
                  
                  
            }
		
		
	}
	
	
}