///<reference path = "../tsDefinitions/phaser.d.ts" />
///<reference path = "../tsDefinitions/phaser-tiled.d.ts" />

module codeTemplate {
	
	export class Preloader extends Phaser.State{
		
		background: Phaser.Color;
		ready: boolean;

		
		preload(){
			
			this.game.time.advancedTiming = true;
			
			// Phaser Way
			this.load.image('16x16', 'assets/16x16.png');
			this.load.tilemap('map', 'assets/map.json', null, Phaser.Tilemap.TILED_JSON);
					
			var cacheKey = Phaser.Plugin.Tiled.utils.cacheKey;
			//this.game.load.tiledmap();
			this.load.tiledmap(cacheKey('map', 'tiledmap'), 'assets/map.json', null, Phaser.Tilemap.TILED_JSON);
			this.load.image(cacheKey('map', 'tileset', '16x16'), 'assets/16x16.png');
			
			
		}
		
		create(){
			
			
			
		}
		
		update(){
			
			this.game.state.start('Sim');
			
			
		}
		
		
	}
	
	
	
}