///<reference path = "../tsDefinitions/phaser.d.ts" />

module codeTemplate {
	
	export class Sim extends Phaser.State{
		
		map: Phaser.Plugin.Tiled.Tilemap;
		layer: Phaser.Plugin.Tiled.Tilelayer;
		tileSet: Phaser.Plugin.Tiled.Tileset;
		
		currentTile: Phaser.Plugin.Tiled.Tile;
		currentTileSprite: PIXI.Texture;
		
		marker: Phaser.Graphics;
		cursors: any;
		
		groupScale: number;
		inputScale: number;
		
		tileMapGroup: Phaser.Group;
		
		groupScaled: boolean;
		
		create(){
			
			this.stage.backgroundColor = "#4DBD33";
			
			this.map = this.add.tiledmap('map');
			this.layer = this.map.layers[0];
			
			this.layer.resizeWorld();
			
<<<<<<< HEAD
			//this.layerPaint = this.map.create('layer', 96, 72, 32, 32 );
			this.layer = this.map.createLayer('layer', 1536, 1152 );

			this.layer.resizeWorld();

			// This holds a Tile from the tilemap's tileset
			// Will be used to "paint" the tilemap
			this.currentTile = new Phaser.Tile(this.layer, 4, 0, 0, 16, 16 );
=======
			console.log(this.map.widthInPixels);
			console.log(this.map.heightInPixels);
			
			console.log(this.layer.widthInPixels);
			console.log(this.layer.heightInPixels);
			
			this.tileSet = this.map.tilesets[0];
		
			this.currentTile = new Phaser.Plugin.Tiled.Tile(this.game, 0, 0, 4, this.tileSet, this.layer);
			this.currentTileSprite = this.map.tilesets[0].getTileTexture(4);
			
			//console.log(this.map.tilesets[0].getTileTexture());
			var sprite = this.map.tilesets[0].getTileTexture(4);
			
			console.log(this.currentTile.tileset.name);
			console.log(this.currentTile.layer.name);
>>>>>>> 4b6fce23a823f9b78a4c8ca2b5126c5e5bc5f416
			
			this.marker = this.add.graphics(0, 0);
			this.marker.lineStyle(2, 0x000000, 1);
			this.marker.drawRect(0, 0, 16, 16);
			this.marker.height = 16;
			this.marker.width = 16;
			
			this.input.addMoveCallback(this.updateMarker, this);
			this.cursors = this.input.keyboard.createCursorKeys();
			
<<<<<<< HEAD
			this.layerScaleGroup = this.add.group();
			this.layerScaleGroup.add(this.layer);
			this.layerScaleGroup.add(this.marker);
			
			this.layerScale = .66;
			this.inputScale = 2.33;
=======
			this.groupScale = 1;
			this.inputScale = 1;
			
			this.tileMapGroup = this.game.add.group();
			//this.tileMapGroup.add(this.map);
			this.tileMapGroup.add(this.layer);
			this.tileMapGroup.add(this.marker);

>>>>>>> 4b6fce23a823f9b78a4c8ca2b5126c5e5bc5f416
			
		}
		
		updateMarker(){
			
			// Draw a Tile Marker and update its position
			// this.marker.x = this.layer.getTileX(this.input.activePointer.x) * 16;
			// this.marker.y = this.layer.getTileY(this.input.activePointer.y) * 16;
			
			var tile = this.layer.getTiles(this.input.activePointer.worldX, this.input.activePointer.worldY, 16, 16);
			
			this.marker.x = tile[0].x;
			this.marker.y = tile[0].y;
			
			if(this.input.mousePointer.isDown){
				
				tile[0].setTexture(this.currentTileSprite);
			
			}

			
		}
		
		update() {
			
			if(this.input.keyboard.isDown(Phaser.Keyboard.Q)){

				this.groupScale -= .05;
				this.inputScale += .05;
				this.groupScaled = true;
	
			}
			if(this.input.keyboard.isDown(Phaser.Keyboard.A)){
				
				this.groupScale += .05;
				this.inputScale -= .05;
				this.groupScaled = true;
				
			}
			
<<<<<<< HEAD
			if(this.scaling === true){
			
				this.layerScale = Phaser.Math.clamp(this.layerScale, .66, 1);
				this.inputScale = Phaser.Math.clamp(this.inputScale, 1, 1.33);
				
				this.layer.setScale(this.layerScale);
				this.marker.scale.set(this.layerScale, this.layerScale);
				
				
				//his.layerScaleGroup.scale.set(this.layerScale);
=======
			if(this.groupScaled === true){
				
				this.groupScale = Phaser.Math.clamp(this.groupScale, .67, 1);
				this.inputScale = Phaser.Math.clamp(this.inputScale, 1, 1.5);
				
				//this.groupScale = Phaser.Math.
				
				this.tileMapGroup.scale.set(this.groupScale);
				//this.layer.scale.set(this.groupScale);
>>>>>>> 4b6fce23a823f9b78a4c8ca2b5126c5e5bc5f416
				this.input.scale.set(this.inputScale);
				
				//this.layer.setupRenderArea();
				this.layer.postUpdate();
				
				this.groupScaled = false;
				
			}
			

			
			if (this.cursors.left.isDown)
		    {
		        this.camera.x -= 20;
				console.log('scrolling');
		    }
		    else if (this.cursors.right.isDown)
		    {
		        this.camera.x += 20;
		    }
		
		    if (this.cursors.up.isDown)
		    {
		        this.camera.y -= 20;
		    }
		    else if (this.cursors.down.isDown)
		    {
		        this.camera.y += 20;
		    }
		}
		
		
		render() {
			
			this.game.debug.cameraInfo(this.camera, 16, 16);
			//this.game.debug.inputInfo( 16, 100);
			//this.game.debug.text("Layer Group: " + this.groupScaleGroup.scale.x, 16, 16);

			var fpsToString = this.game.time.fps;
			this.game.debug.text(fpsToString.toString(), 2, 14, "#00ff00" )
	        //this.game.debug.cameraInfo(this.game.camera, 32, 32);
			
			
		}
		
	}
	
	
}